# Prototype of using React Dazzle w/Redux For Concierge Dashboard Templates

[Dazzle Prototype Docs](https://madmobile.atlassian.net/wiki/display/~tsmart/Dazzle+Prototype)

## Synopsis;

we've identified an open source react dashboarding library known as [React Dazzle](https://github.com/Raathigesh/Dazzle)

that library ships with a sample dashboard for known as [Dazzle Starter Kit](https://github.com/Raathigesh/Dazzle-Starter-Kit)

we've enhanced that Dazzle Starter Kit to use redux state management for our purposes to test whether it suits our "Dashboard Templating" needs

dashboard templating seeks to provide the means for a configuration-driven approach to dashboard widgets and layout

## Usage

- run `npm install`
- run `npm run dev`

Visit `localhost:8080` and you should see the sample running.

## Demo hints:

- click the 'edit' button to enable dashboard configuration
- widgets can be added to target zones
- widgets can be removed
- widgets can be dragged and dropped to any target zone
- you can add as many instances of widgets as you want to any target zone