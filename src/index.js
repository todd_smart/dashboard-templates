import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Dashboard from './components/Dashboard';
import {createStore} from 'redux';
import template from './reducers';

const templateStore = createStore(template, window.devToolsExtension && window.devToolsExtension());

/**
 * dispatches an action to the reducer to update the template config with the provided layout
 *
 * @param {object} layout The template layout
 */
function updateLayout(layout) {
  templateStore.dispatch({
    type: 'UPDATE',
    layout
  });
}

function render() {
  ReactDOM.render(
    <Dashboard template={templateStore.getState()} updateLayout={updateLayout} />,
    document.getElementById('root')
  );
}

render();
templateStore.subscribe(render);
