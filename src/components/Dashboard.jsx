import React, { Component, PropTypes } from 'react';
import Dashboard, { addWidget } from 'react-dazzle';

// App components
import Header from './Header';
import EditBar from './EditBar';
import Container from './Container';
import AddWidgetDialog from './AddWidgetDialog';
import CustomFrame from './CustomFrame';

// Widgets of the dashboard.
import BarChart from './widgets/BarChart';
import LineChart from './widgets/LineChart';
import DoughnutChart from './widgets/DoughnutChart';

// We are using bootstrap as the UI library
import 'bootstrap/dist/css/bootstrap.css';

// Default styes of dazzle.
import 'react-dazzle/lib/style/style.css';

// Our styles
import '../styles/custom.css';

class App extends Component {

  /**
   * propTypes
   *
   * @property {object} template The dashboard template (includes the widgets and layout)
   */
  static propTypes = {
    template: PropTypes.object,
    updateLayout: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    /**
     * state
     *
     * TODO: tsmart 08262026 - document these
     *
     * @type {object}
     *
     * @property {boolean} editMode
     * @property {boolean} isModalOpen
     * @property {object} addWidgetOptions
     */
    this.state = {
      editMode: false,
      isModalOpen: false,
      addWidgetOptions: null
    };
  }

  /**
   * Open the AddWidget dialog by setting the 'isModalOpen' to true.
   * Also preserve the details such as the layout, rowIndex, and columnIndex  in 'addWidgetOptions'.
   *  This will be used later when user picks a widget to add.
   */
  onAddRequest = (layout, rowIndex, columnIndex) => {
    this.setState({
      isModalOpen: true,
      addWidgetOptions: {
        layout,
        rowIndex,
        columnIndex,
      },
    });
  };

  /**
   * When user selects a widget from the modal dialog, this will be called.
   * By calling the 'addWidget' method, the widget could be added to the previous requested location.
   */
  addWidget = (widgetName) => {
    const {layout, rowIndex, columnIndex} = this.state.addWidgetOptions;

    /**
     * 'AddWidget' method gives you the new layout.
     */
    this.props.updateLayout(addWidget(layout, rowIndex, columnIndex, widgetName));

    // Close the add widget modal
    this.onRequestClose();
  };

  /**
   * When a widget is removed, the layout should be set again.
   */
  onRemove = (layout) => {
    this.props.updateLayout(layout);
  };

  /**
   * When a widget moved, this will be called. Layout should be given back.
   */
  onMove = (layout) => {
    this.props.updateLayout(layout);
  };

  /**
   * This will be called when user tries to close the modal dialog.
   */
  onRequestClose = () => {
    this.setState({
      isModalOpen: false,
    });
  };

  /**
   * Toggeles edit mode in dashboard.
   */
  toggleEdit = () => {
    this.setState({
      editMode: !this.state.editMode,
    });
  };

  /**
   * takes a widget configuration and returns the actual widget components for the dashboard
   *
   * @param {Object} widgetConfig The configuration for the widgets (see shape in reducer)
   * @returns {{}} A set of dashboard widgets
   */
  getWidgetComponents = (widgetConfig) => {
    let widgetComponents = {};

    // loop over the config and generate each widget component
    for (const widget in widgetConfig) {
      if(widgetConfig.hasOwnProperty(widget)) {

        // the widget component begins as a shallow copy of its provided widget config
        let widgetComponent = {
          ...widgetConfig[widget]
        };

        // replace the widget component's type with the actual widget
        switch (widgetComponent.type) {
          case 'BarChart':
            widgetComponent.type = BarChart;
            break;
          case 'DoughnutChart':
            widgetComponent.type = DoughnutChart;
            break;
          case 'LineChart':
            widgetComponent.type = LineChart;
            break;
          default:
            widgetComponent.type = <div/>;
            break;
        }
        widgetComponents[widget] = widgetComponent;
      }
    }
    return widgetComponents;
  };

  render() {
    return (
    <Container>
      <AddWidgetDialog widgets={this.props.template.widgets} isModalOpen={this.state.isModalOpen} onRequestClose={this.onRequestClose} onWidgetSelect={this.addWidget}/>
      <Header />
      <EditBar onEdit={this.toggleEdit} />
      <Dashboard
        frameComponent={CustomFrame}
        onRemove={this.onRemove}
        layout={this.props.template.layout}
        widgets={this.getWidgetComponents(this.props.template.widgets)}
        editable={this.state.editMode}
        onAdd={this.onAddRequest}
        onMove={this.onMove}
        addWidgetComponentText="Add New Widget"
        />

    </Container>
    );
  }
}

export default App;
