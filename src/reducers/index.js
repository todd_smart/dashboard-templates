const initialState = {
  // Widgets that are available in the dashboard
  widgets: {
    EngineTelemetricsWidget: {
      type: 'BarChart',
      title: 'Engine'
    },
    PerformanceWidget: {
      type: 'DoughnutChart',
      title: 'Reactor Temp'
    },
    ShipVitalTelemetricsWidget: {
      type: 'LineChart',
      title: 'Reactor Telemetrics'
    }
  },
  // Layout of the dashboard
  layout: {
    rows: [{
      columns: [{
        className: 'col-md-12 col-sm-12 col-xs-12',
        widgets: [{key: 'ShipVitalTelemetricsWidget'}]
      }]
    }, {
      columns: [{
        className: 'col-md-8 col-sm-8 col-xs-8',
        widgets: [{key: 'EngineTelemetricsWidget'}]
      }, {
        className: 'col-md-4 col-sm-4 col-xs-4',
        widgets: [{key: 'PerformanceWidget'}]
      }]
    }]
  }
};

export default function template(state = initialState, action) {
  switch (action.type) {
  case 'INIT':
    // TODO: tsmart 08262017 - use persisted state if it exists
    return {
      ...state
    };
  case 'UPDATE':
    return {
      ...state,
      layout: action.layout
    };
  default:
    return state;
  }
}
